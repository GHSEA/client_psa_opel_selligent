/**
 * NAME: SELLIGENT - Iframe listener - All Pages
 * postMessage listener script: 
 *  allowing iframe messages to be received & translated to dataLayer events
 *  allowing event to originate from gm.emsecure.net (other events will be ignored)
 * 
 *  ! Script is active but only triggered when a post message is taking place
 */

<script>
window.addEventListener('message', function(e) {
    if (e.origin === 'https://gm.emsecure.net') {
        evObj = e.data[0]
        window.dataLayer.push(evObj);
    }
}, false);
</script>  