/**
 * NAME: SELLIGENT - SITE - Selligent_details-variables
 * Selligent Site configuration variables: 
 *  variables loading:
 *      QUANTUM_AANDRIJVING => regex table - AANDRIJVING
 *      QUANTUM_AUTO_CATEGORIE => regex table - AUTO CATEGORIE
 *      QUANTUM_CTA => regex table - CTA
 *      QUANTUM_INTERESSES => regex table - INTERESSES
 *      QUANTUM_INTERESSE_SUBTYPE => regex table - INTERESSE SUBTYPE
 *      QUANTUM_MOI => regex table - MOI
 *      QUANTUM_BODY_STYLE => regex table - MOI
 *      QUANTUM_OPEL_BEZIT => regex table - OPEL BEZIT
 *      QUANTUM_SOORT_AUTO => regex table - SOORT AUTO
 *      QUANTUM_TYPE_RIJDER => regex table - TYPE RIJDER
 *      QUANTUM_URL => js - Full URL
 *      QUANTUM_TOUCHPOINTS => regex table - TOUCHPOINTS
 *  initiating queue:
 *      isEvent => true,
        isTargeting => true
 * 
 *  ! Scripts needs to loading on all pages
 */
<script>
        wa.bt_queue.push({
            "tags": [{
                "tag": "QUANTUM_AANDRIJVING",
                "value": {{regex table - AANDRIJVING}}
            }, {
                "tag": "QUANTUM_AUTO_CATEGORIE",
                "value": {{regex table - AUTO CATEGORIE}}
            }, {
                "tag": "QUANTUM_CTA",
                "value": {{regex table - CTA}}
            }, {
                "tag": "QUANTUM_INTERESSES",
                "value": {{regex table - INTERESSES}}
            }, {
                "tag": "QUANTUM_INTERESSE_SUBTYPE",
                "value": {{regex table - INTERESSE SUBTYPE}}
            }, {
                "tag": "QUANTUM_MOI",
                "value": {{regex table - MOI}}
            }, {
                "tag": "QUANTUM_BODY_STYLE",
                "value": {{regex table - BODYSTYLE}}
            }, {
                "tag": "QUANTUM_OPEL_BEZIT",
                "value": {{regex table - OPEL BEZIT}}
            }, {
                "tag": "QUANTUM_SOORT_AUTO",
                "value": {{regex table - SOORT AUTO}}
            }, {
                "tag": "QUANTUM_TYPE_RIJDER",
                "value": {{regex table - TYPE RIJDER}}
            }, {
                "tag": "QUANTUM_URL",
                "value": {{js - Full URL}}
            }, {
                "tag": "QUANTUM_TOUCHPOINTS",
                "value": {{regex table - TOUCHPOINTS}}
            }],
            "isEvent": true,
            "isTargeting": true
        })
</script>