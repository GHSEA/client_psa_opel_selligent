/**
 * NAME: Custom HTML - Selligent Debug
 * debugger script: 
 *  allowing items to be printed in development modus only
 *  allowing you to check all the items
 *  making use of macros
 *    {{regex table - AANDRIJVING}}
 *    {{regex table - AUTO CATEGORIE}}
 *    {{regex table - BODYSTYLE}}
 *    {{regex table - CTA}}
 *    {{regex table - INTERESSE SUBTYPE}}
 *    {{regex table - INTERESSES}}
 *    {{regex table - MOI}}
 *    {{regex table - OPEL BEZIT}}
 * 
 *  ! Script is only active in preview mode
 */

<script>
  var _selObj = {};
  _selObj.AANDRIJVING = {{regex table - AANDRIJVING}};
  _selObj['AUTO CATEGORIE'] = {{regex table - AUTO CATEGORIE}};
  _selObj.BODYSTYLE = {{regex table - BODYSTYLE}};
  _selObj.CTA = {{regex table - CTA}};
  _selObj['INTERESSE SUBTYPE'] = {{regex table - INTERESSE SUBTYPE}};
  _selObj.INTERESSES = {{regex table - INTERESSES}};
  _selObj.MOI = {{regex table - MOI}};
  _selObj['OPEL BEZIT'] = {{regex table - OPEL BEZIT}};
  _selObj['SOORT AUTO'] = {{regex table - SOORT AUTO}};
  _selObj.TOUCHPOINTS = {{regex table - TOUCHPOINTS}};
  _selObj['TYPE RIJDER'] = {{regex table - TYPE RIJDER}};
  console.table(_selObj);
</script>