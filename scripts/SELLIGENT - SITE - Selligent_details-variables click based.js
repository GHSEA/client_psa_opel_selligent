/**
 * NAME: SELLIGENT - SITE - Selligent_details-variables
 * Selligent Site configuration variables: 
 *  variables loading:
 *      QUANTUM_AANDRIJVING => regex table - AANDRIJVING
 *      QUANTUM_AUTO_CATEGORIE => regex table - AUTO CATEGORIE
 *      QUANTUM_CTA => regex table - CTA
 *      QUANTUM_INTERESSES => regex table - INTERESSES
 *      QUANTUM_INTERESSE_SUBTYPE => regex table - INTERESSE SUBTYPE
 *      QUANTUM_MOI => regex table - MOI
 *      QUANTUM_BODY_STYLE => regex table - MOI
 *      QUANTUM_OPEL_BEZIT => regex table - OPEL BEZIT
 *      QUANTUM_SOORT_AUTO => regex table - SOORT AUTO
 *      QUANTUM_TYPE_RIJDER => regex table - TYPE RIJDER
 *      QUANTUM_URL => js - Full URL
 *      QUANTUM_TOUCHPOINTS => regex table - TOUCHPOINTS
 *  initiating queue:
 *      isEvent => true,
        isTargeting => true
 *  track renderer:
        universeId
        profile
        profileId
        url
        referer
        tagValues
        exposedFields
        allowCookies
        allowLocalStorage 
        pageTitle
        metaTags
 * 
 *  ! Scripts needs to loading on all pages
 */
<script>
        wa.bt_queue.push({
            "tags": [{
                "tag": "QUANTUM_AANDRIJVING",
                "value": {{regex table - AANDRIJVING}}
            }, {
                "tag": "QUANTUM_AUTO_CATEGORIE",
                "value": {{regex table - AUTO CATEGORIE}}
            }, {
                "tag": "QUANTUM_CTA",
                "value": {{regex table - CTA}}
            }, {
                "tag": "QUANTUM_INTERESSES",
                "value": {{regex table - INTERESSES}}
            }, {
                "tag": "QUANTUM_INTERESSE_SUBTYPE",
                "value": {{regex table - INTERESSE SUBTYPE}}
            }, {
                "tag": "QUANTUM_MOI",
                "value": {{regex table - MOI}}
            }, {
                "tag": "QUANTUM_BODY_STYLE",
                "value": {{regex table - BODYSTYLE}}
            }, {
                "tag": "QUANTUM_OPEL_BEZIT",
                "value": {{regex table - OPEL BEZIT}}
            }, {
                "tag": "QUANTUM_SOORT_AUTO",
                "value": {{regex table - SOORT AUTO}}
            }, {
                "tag": "QUANTUM_TYPE_RIJDER",
                "value": {{regex table - TYPE RIJDER}}
            }, {
                "tag": "QUANTUM_URL",
                "value": {{js - Full URL}}
            }, {
                "tag": "QUANTUM_TOUCHPOINTS",
                "value": {{regex table - TOUCHPOINTS}}
            }],
            "isEvent": true,
            "isTargeting": true
        })

function reqListener() {
    var e = JSON.parse(this.responseText);
    e = e.$$profileId;
    var f = {{regex table - MOI}}
    var t = {{regex table - TOUCHPOINTS - click based}}
    e = "https://gm.emsecure.net/renderers/content/Json/Ji6f%2BPM6Hq1bBuh7Wv3BwRTyC95Y1Lx9bWaHNG0_JAPafaw5E3TC8%2BGOxLkmThdmj_DaYxMeGwg4Jh?SITEID=" + e + "&TOUCHPOINT=" + t + "&MOI=" + f, (t = new XMLHttpRequest).open("POST", e), t.send()
}
var api = "https://sitep.slgnt.eu/api/track",
    oReq = new XMLHttpRequest;
oReq.addEventListener("load", reqListener), oReq.open("POST", api), oReq.send(JSON.stringify({
    universeId: "69b89fb5-d21e-431c-8684-8e1fa606ca89",
    profile: null,
    profileId: BT.getProfileId(),
    url: "https://www.opel.nl",
    referer: "",
    tagValues: [],
    exposedFields: [{
        field: "Profile_id"
    }],
    allowCookies: !0,
    allowLocalStorage: !1,
    pageTitle: "",
    metaTags: {}
}));
</script>